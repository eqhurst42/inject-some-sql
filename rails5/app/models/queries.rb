Queries = [
  {
    :action => :delete_all,
    :name => "Delete All Method [FIXED]",
    :link => "http://api.rubyonrails.org/v5.0.0.1/classes/ActiveRecord/Relation.html#method-i-delete_all",
    :query => 'User.delete_all(id: params[:id])',
    :input => {:name => :id, :example => '1) OR 1=1--'},
    :example => "This example bypasses any conditions and deletes all users.",
    :desc => <<-MD
Any methods which delete records should be used with care!

The `delete_all` method takes the same kind of conditions arguments as `find`.
The argument can be a string, an array, or a hash of conditions. Strings will not
be escaped at all. Use an array or hash to safely parameterize arguments.

Never pass user input directly to `delete_all`.
    MD
  },


  {
    :action => :find_by,
    :name => "Find By Method [DON'T USE THIS AT ALL]",
    :link => "http://api.rubyonrails.org/v5.0.0.1/classes/ActiveRecord/FinderMethods.html#method-i-find_by",
    :query => 'User.find_by params[:id]',
    :input => {:name => :id, :example => "admin = 't'"},
    :example => 'This will find users who are admins.',
    :desc => <<-MD
Added in Rails 4, the `find_by`/`find_by!` methods are simply calling `where(*args).take`, so all the options for `where` also apply.

The safest (and most common) use of these methods is to pass in a hash table.
    MD
  },

  {
    :action => :from_method,
    :name => "From Method [FIXED]",
    :link => "http://api.rubyonrails.org/v5.0.0.1/classes/ActiveRecord/QueryMethods.html#method-i-from",
    :query => "User.from(ActiveRecord::Base::string(params[:from])).where(admin: false)",
    :input => {:name => :from, :example => "users WHERE admin = 't' OR 1=?;"},
    :example => "Instead of returning all non-admin users, we return all admin users.",
    :desc => <<-MD
The `from` method accepts arbitrary SQL.
    MD
  },

  {
    :action => :where,
    :name => "Where Method [FIXED]",
    :link => "http://api.rubyonrails.org/v5.0.0.1/classes/ActiveRecord/QueryMethods.html#method-i-where",
    :query => 'User.where(:name => params[:name]).where(:password => params[:password]).first',
    :input => {:name => :name, :example => "') OR 1--"},
    :example => 'The example below is using classic SQL injection to bypass authentication.',
    :desc => <<-MD
The `where` method can be passed a straight SQL string. Calls using a hash of name-value pairs are escaped, and the array form can be used for safely parameterizing queries.
    MD
  },

  {
    :action => :update_all_method,
    :name => "Update All Method [FIXED / DON'T USE THIS AT ALL]",
    :link => "http://api.rubyonrails.org/v5.0.0.1/classes/ActiveRecord/Relation.html#method-i-update_all",
    :query => 'User.update_all("admin = 1 WHERE name LIKE \'% :name => params[:name] %\'")',
    :input => {:name => :name, :example => '\' OR 1=1;'},
    :example => "Update every user to be an admin.",
    :desc => <<-MD
Like `delete_all`, `update_all` accepts any SQL as a string.

User input should never be passed directly to `update_all`, only as values in a hash table.
    MD
  },

]
